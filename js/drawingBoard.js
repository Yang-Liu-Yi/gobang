document.addEventListener("DOMContentLoaded", function () {

    //定义生成棋盘的方法
    function drawingBoard(data = {
        //棋盘默认属性
        n: 9,//棋格单轴个数
        width: 500,//棋盘宽度
        height: 500,//棋盘高度
        color: "d2a143"//棋盘颜色
    }) {
        //获取棋盘元素
        var board = document.querySelector(".draw-board");
        if (board == null) {
            return;
        }

        //解构赋值获取设置的属性
        const { n, width, height, color } = data;

        for (var i = 0; i < n * n; i++) {

            //创建棋格
            var grid = document.createElement("div");
            //设置棋格为CSS3盒子模型
            grid.style.boxSizing = "border-box";

            //设置棋格边框
            grid.style.float = "left";
            grid.style.width = width / 10 + "px";
            grid.style.height = height / 10 + "px";
            grid.style.backgroundColor = "#" + color;
            grid.style.borderLeft = "1px solid #000";
            grid.style.borderTop = "1px solid #000";

            //设置棋盘外边框
            if (i % n == 0) {
                grid.style.borderLeft = "3px solid #000";
            }

            if (i % n == n - 1) {
                grid.style.borderRight = "3px solid #000";
            }

            if (i <= n - 1) {
                grid.style.borderTop = "3px solid #000";
            }

            if (i >= n * (n - 1)) {
                grid.style.borderBottom = "3px solid #000";
            }

            //设置棋盘样式
            board.style.width = width + "px";
            board.style.height = height + "px";
            board.style.boxSizing = "border-box";
            board.style.padding = width / 20 + "px";
            board.style.backgroundColor = "#" + color;

            //把棋格添加到棋盘
            board.appendChild(grid);
        }
    }

    //绘制棋盘
    drawingBoard();
});





