document.addEventListener("DOMContentLoaded", function () {

    //获取按钮
    var btn = document.querySelectorAll(".btn");

    for (let i = 0; i < btn.length; i++) {
        //计时器
        var timer;
        btn[i].addEventListener("mouseover", function () {
            this.style.backgroundColor = "#2c8d48";
        });

        btn[i].addEventListener("mouseout", function () {
            //鼠标离开按钮前清除定时器,防止在定时器执行过程中鼠标离开导致按钮颜色未改变
            clearTimeout(timer);
            this.style.backgroundColor = "#5FB878";
        });

        btn[i].addEventListener("click", function () {
            //单击按钮前清除定时器,防止点击过快
            clearTimeout(timer);
            this.style.backgroundColor = "#5FB878";
            timer = setTimeout(() => {
                this.style.backgroundColor = "#2c8d48";

            }, 120);
        });
    }

});
